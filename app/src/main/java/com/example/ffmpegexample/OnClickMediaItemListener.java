package com.example.ffmpegexample;

import java.io.File;

public interface OnClickMediaItemListener {
    void onClickFile(File file);
}
