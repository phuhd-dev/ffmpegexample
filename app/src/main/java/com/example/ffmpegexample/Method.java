package com.example.ffmpegexample;

import android.util.Log;

import java.io.File;

public class Method {
    public static void loadDirectoryFiles(File directory) {
        File[] fileList = directory.listFiles();
        if (fileList != null && fileList.length > 0) {
            for (int index = 0; index < fileList.length; index++) {
                if (fileList[index].isDirectory()) {
                    loadDirectoryFiles(fileList[index]);
                } else {
                    String name = fileList[index].getName().toLowerCase();

                    for (String extension : Constant.videoExtensions) {
                        if (name.endsWith(extension)) {
                            Constant.allMediaList.add(fileList[index]);
                            //when we found file
                            break;
                        }
                    }
                }
            }
        }
    }
}
