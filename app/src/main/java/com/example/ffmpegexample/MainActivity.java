package com.example.ffmpegexample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    // list of all permission for app
    String[] allPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private static final int PERMISSIONS_REQUEST_CODE = 483;

    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_camera_roll_button:
                onClickCameraRoll();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;

            for (int index = 0; index < grantResults.length; index++) {
                if (grantResults[index] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[index], grantResults[index]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                goToMediaSelectionActivity();
            }
        }
    }

    public AlertDialog showDialog(String title,
                                  String message,
                                  String positiveLabel,
                                  DialogInterface.OnClickListener positiveOnClick,
                                  String negativeLabel,
                                  DialogInterface.OnClickListener negativeOnClick,
                                  boolean isCancelAble) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(isCancelAble);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);

        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    private void onClickCameraRoll() {
        if (checkAndRequestPermissions()) {
            goToMediaSelectionActivity();
        }
    }

    private void goToMediaSelectionActivity() {
        Intent intent = new Intent(MainActivity.this, MediaSelectionActivity.class);
        startActivity(intent);
    }

    private boolean checkAndRequestPermissions() {
        List<String> permissionsNeededList = new ArrayList<>();
        for (String permission : allPermissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsNeededList.add(permission);
            }
        }

        if (!permissionsNeededList.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsNeededList.toArray(new String[permissionsNeededList.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }
}
