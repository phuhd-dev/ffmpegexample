package com.example.ffmpegexample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.arthenica.mobileffmpeg.FFprobe;
import com.arthenica.mobileffmpeg.MediaInformation;

import java.io.File;

public class MediaSelectionActivity extends AppCompatActivity implements OnClickMediaItemListener {
    private String[] allPaths;
    private File storage;

    Toolbar mToolbar;
    RecyclerView mRecyclerView;
    ContentLoadingProgressBar mContentLoadingBar;

    MediaAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_selection);

        Constant.allMediaList.clear();

        mapView();
        initToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadingMedia();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void loadingMedia() {
        mContentLoadingBar.setVisibility(View.VISIBLE);

        allPaths = StorageUtil.getStorageDirectories(this);
        for (String path : allPaths) {
            storage = new File(path);
            Method.loadDirectoryFiles(storage);
        }

        mContentLoadingBar.setVisibility(View.GONE);

        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        //if you face lack in scrolling then add following lines
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setNestedScrollingEnabled(false);

        mAdapter = new MediaAdapter(this, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void mapView() {
        mToolbar = findViewById(R.id.activity_media_selection_toolbar);
        mRecyclerView = findViewById(R.id.activity_media_selection_recycler_view);
        mContentLoadingBar = findViewById(R.id.activity_media_selection_content_loading_progress_bar);
    }

    @Override
    public void onClickFile(File file) {
        MediaInformation info = FFprobe.getMediaInformation(file.getPath());
        Log.d("AAA", "path : " + info.getPath());
        Log.d("AAA", "raw information : " + info.getRawInformation());
        Log.d("AAA", "format : " + info.getFormat());
    }
}
